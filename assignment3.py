## Assignment 3, Computational Game Theory
## Made by Yelda Büyük, 0527366
## Vrije Universiteit Brussel
## 2020-2021

import numpy as np
from random import choice, uniform
import matplotlib.pyplot as plt

##################
## Exercise 1.1 ##
##################

# Mean array, corresponds to the actions.
mean_arr = [2.2, 1.2, 1.5, 2.0]

# Standard deviation array, corresponds to the actions.
standard_deviation_arr = [2.2, 4, 1, 1.2]

# Results for random.
result_random = np.zeros(1000)
result_r = [] # End result

# Results for greedy.
greedy = np.zeros(1000)
greedy_01 = np.zeros(1000)
greedy_02 = np.zeros(1000)
greedy_r = []
greedy_01_r = []
greedy_02_r = []

#Results for softmax
soft_1 = np.zeros(1000)
soft_1_r = []
soft_01 = np.zeros(1000)
soft_01_r = []

# Results for new algos
time_greed = np.zeros(1000)
time_greed_r = []
soft_maxtime = np.zeros(1000)
soft_maxtime_r = []

#### GRAPH PER ARM RESULTS
arm1_random = np.zeros(1000) # Random
arm2_random = np.zeros(1000)
arm3_random = np.zeros(1000)
arm4_random = np.zeros(1000)

arm1_random_r = []
arm2_random_r = []
arm3_random_r = []
arm4_random_r = []

arm1_greedy= np.zeros(1000) # Greedy 0
arm2_greedy = np.zeros(1000)
arm3_greedy = np.zeros(1000)
arm4_greedy = np.zeros(1000)

arm1_greedy_r = []
arm2_greedy_r = []
arm3_greedy_r = []
arm4_greedy_r = []

arm1_greedy_01 = np.zeros(1000) # Greedy 0.1
arm2_greedy_01 = np.zeros(1000)
arm3_greedy_01 = np.zeros(1000)
arm4_greedy_01 = np.zeros(1000)

arm1_greedy_01_r = []
arm2_greedy_01_r = []
arm3_greedy_01_r = []
arm4_greedy_01_r = []


arm1_greedy_02 = np.zeros(1000) # Greedy 0.2
arm2_greedy_02 = np.zeros(1000)
arm3_greedy_02 = np.zeros(1000)
arm4_greedy_02 = np.zeros(1000)

arm1_greedy_02_r = []
arm2_greedy_02_r = []
arm3_greedy_02_r = []
arm4_greedy_02_r = []

arm1_softmax_1 = np.zeros(1000) # Softmax 1
arm2_softmax_1 = np.zeros(1000)
arm3_softmax_1 = np.zeros(1000)
arm4_softmax_1 = np.zeros(1000)

arm1_softmax_1_r = []
arm2_softmax_1_r = []
arm3_softmax_1_r = []
arm4_softmax_1_r = []


arm1_softmax_01 = np.zeros(1000) # Softmax 0.1
arm2_softmax_01 = np.zeros(1000)
arm3_softmax_01 = np.zeros(1000)
arm4_softmax_01 = np.zeros(1000)

arm1_softmax_01_r = []
arm2_softmax_01_r = []
arm3_softmax_01_r = []
arm4_softmax_01_r = []

arm1_timegreed = np.zeros(1000) # Time greedy
arm2_timegreed = np.zeros(1000)
arm3_timegreed = np.zeros(1000)
arm4_timegreed = np.zeros(1000)

arm1_timegreed_r = []
arm2_timegreed_r = []
arm3_timegreed_r = []
arm4_timegreed_r = []

arm1_soft_maxtime = np.zeros(1000) # Softmax time varying
arm2_soft_maxtime = np.zeros(1000)
arm3_soft_maxtime = np.zeros(1000)
arm4_soft_maxtime = np.zeros(1000)

arm1_soft_maxtime_r= []
arm2_soft_maxtime_r = []
arm3_soft_maxtime_r= []
arm4_soft_maxtime_r= []

# Histogram shizzle.
random_h =  [0,0,0,0]
greedy_h = [0,0,0,0]
greedy_01_h = [0,0,0,0]
greedy_02_h = [0,0,0,0]
softmax_1_h = [0,0,0,0]
softmax_01_h = [0,0,0,0]
timegreed_h = [0,0,0,0]
softmax_time_h = [0,0,0,0]


# Random n bandit algorithm.
def random_n_bandit(): 
    for i in range(30):
        # Amount of times an action has been chosen.
        chosen_action = [0,0,0,0]
        # Action reward.
        action_reward = [0,0, 0, 0]
        # Q_a table
        Q_a = [0,0,0,0]
        cum_result = 0
        for i in range(1000):
            A_t = choice(range(4))
            mean = mean_arr[A_t]
            standard_deviation = standard_deviation_arr[A_t]
            reward = np.random.normal(mean, standard_deviation, 1)
            # Update 
            chosen_action[A_t] = chosen_action[A_t] + 1
            action_reward[A_t] = action_reward[A_t] + reward[0]
            Q_a[A_t] = action_reward[A_t] / chosen_action[A_t]
            # For average reward measurement.
            cum_result = reward[0] + cum_result
            result_random[i] = result_random[i] + cum_result
            # For actual Qa estimate over time
            arm1_random[i] = arm1_random[i] + Q_a[0]
            arm2_random[i] = arm2_random[i] + Q_a[1]
            arm3_random[i] = arm3_random[i] + Q_a[2]
            arm4_random[i] = arm4_random[i] + Q_a[3]
        # Update action selected number
        for s in range(4):
            random_h[s] = random_h[s] + chosen_action[s]


# Greedy n bandit algorithm, takes an episolon value as the parameter.
# If random number < episolon, we choose a random action.
# If not, we choose the max reward.
def greedy_n_bandit(episolon, target, arm1, arm2, arm3, arm4, histogram):
    for i in range(30):
        # Amount of times an action has been chosen.
        chosen_action = [0,0,0,0]
        # Action reward.
        action_reward = [0,0, 0, 0]
        # Q_a table
        Q_a = [0,0,0,0]
        cum_result = 0
        for i in range(1000):
            # Random chance
            chance = uniform(0,1)
            if chance <= episolon:
                A_t = choice(range(4))
                mean = mean_arr[A_t]
                standard_deviation = standard_deviation_arr[A_t]
                reward = np.random.normal(mean, standard_deviation, 1)
                # Update 
                chosen_action[A_t] = chosen_action[A_t] + 1
                action_reward[A_t] = action_reward[A_t] + reward[0]
                Q_a[A_t] = action_reward[A_t] / chosen_action[A_t]
                # For average reward measurement.
                cum_result = reward[0] + cum_result
                target[i] = target[i] + cum_result
                # For actual Qa estimate over time
                arm1[i] = arm1[i] + Q_a[0]
                arm2[i] = arm2[i] + Q_a[1]
                arm3[i] = arm3[i] + Q_a[2]
                arm4[i] = arm4[i] + Q_a[3]
            else:
                mean = max(mean_arr)
                A_t = mean_arr.index(mean)
                standard_deviation = standard_deviation_arr[A_t]
                reward = np.random.normal(mean, standard_deviation, 1)
                # Update 
                chosen_action[A_t] = chosen_action[A_t] + 1
                action_reward[A_t] = action_reward[A_t] + reward[0]
                Q_a[A_t] = action_reward[A_t] / chosen_action[A_t]
                # For average reward measurement.
                cum_result = reward[0] + cum_result
                target[i] = target[i] + cum_result
                # For actual Qa estimate over time
                arm1[i] = arm1[i] + Q_a[0]
                arm2[i] = arm2[i] + Q_a[1]
                arm3[i] = arm3[i] + Q_a[2]
                arm4[i] = arm4[i] + Q_a[3]
        # Update action selected number
        for s in range(4):
            histogram[s] = histogram[s] + chosen_action[s]
        
# Soft max algorithm, takes a value t as parameter.
def soft_max(t, target, arm1, arm2, arm3, arm4, histogram):
    for i in range(30):
        # Amount of times an action has been chosen.
        chosen_action = [0,0,0,0]
        # Action reward.
        action_reward = [0,0, 0, 0]
        #Action_selection
        action_s = [0,0,0,0]
        # Q_a table
        Q_a = [0,0,0,0]
        cum_result = 0
        for i in range(1000):
            denominator = 0
            for d in range(4):
                denominator = denominator + np.exp(Q_a[d]/t)
            for a in range(4):
                action_s[a] = (np.exp(Q_a[a]/t)) / denominator
            ## Select action.
            # Random chance.
            chance = uniform(0,1)
            begin = 0
            r = 0
            for j in range(4):
                end = begin + action_s[j]
                if ((chance >= begin) and (chance <= end)):
                    mean = mean_arr[j]
                    standard_deviation = standard_deviation_arr[j]
                    reward = np.random.normal(mean, standard_deviation, 1)
                    r = reward[0]
                    # Update
                    chosen_action[j] = chosen_action[j] + 1
                    action_reward[j] = action_reward[j] + reward[0]
                    Q_a[j] = action_reward[j] / chosen_action[j]
                begin = end
            # For average reward measurement.
            cum_result = r + cum_result
            target[i] = target[i] + cum_result
            # For actual Qa estimate over time
            arm1[i] = arm1[i] + Q_a[0]
            arm2[i] = arm2[i] + Q_a[1]
            arm3[i] = arm3[i] + Q_a[2]
            arm4[i] = arm4[i] + Q_a[3]
        # Update action selected number
        for s in range(4):
            histogram[s] = histogram[s] + chosen_action[s]
                
# NEW ALGOS
# Greedy n bandit algorithm, takes an episolon value as the parameter.
# If random number < episolon, we choose a random action.
# If not, we choose the max reward.
def time_greedy(target, arm1, arm2, arm3, arm4, histogram):
    for i in range(30):
        # Amount of times an action has been chosen.
        chosen_action = [0,0,0,0]
        # Action reward.
        action_reward = [0,0, 0, 0]
        # Q_a table
        Q_a = [0,0,0,0]
        cum_result = 0
        for i in range(1000):
            # Random chance
            chance = uniform(0,1)
            k = i + 1
            episolon = 1 / (np.sqrt(k))
            if chance <= episolon:
                A_t = choice(range(4))
                mean = mean_arr[A_t]
                standard_deviation = standard_deviation_arr[A_t]
                reward = np.random.normal(mean, standard_deviation, 1)
                # Update 
                chosen_action[A_t] = chosen_action[A_t] + 1
                action_reward[A_t] = action_reward[A_t] + reward[0]
                Q_a[A_t] = action_reward[A_t] / chosen_action[A_t]
                # For average reward measurement.
                cum_result = reward[0] + cum_result
                target[i] = target[i] + cum_result
                # For actual Qa estimate over time
                arm1[i] = arm1[i] + Q_a[0]
                arm2[i] = arm2[i] + Q_a[1]
                arm3[i] = arm3[i] + Q_a[2]
                arm4[i] = arm4[i] + Q_a[3]
            else:
                mean = max(mean_arr)
                A_t = mean_arr.index(mean)
                standard_deviation = standard_deviation_arr[A_t]
                reward = np.random.normal(mean, standard_deviation, 1)
                # Update 
                chosen_action[A_t] = chosen_action[A_t] + 1
                action_reward[A_t] = action_reward[A_t] + reward[0]
                Q_a[A_t] = action_reward[A_t] / chosen_action[A_t]
                # For average reward measurement.
                cum_result = reward[0] + cum_result
                target[i] = target[i] + cum_result
                # For actual Qa estimate over time
                arm1[i] = arm1[i] + Q_a[0]
                arm2[i] = arm2[i] + Q_a[1]
                arm3[i] = arm3[i] + Q_a[2]
                arm4[i] = arm4[i] + Q_a[3]
        # Update action selected number
        for s in range(4):
            histogram[s] = histogram[s] + chosen_action[s]

# SOFTMAX TIME VARYING
def soft_max_time(target, arm1, arm2, arm3, arm4, histogram):
    for i in range(30):
        # Amount of times an action has been chosen.
        chosen_action = [0,0,0,0]
        # Action reward.
        action_reward = [0,0, 0, 0]
        #Action_selection
        action_s = [0,0,0,0]
        # Q_a table
        Q_a = [0,0,0,0]
        cum_result = 0
        for i in range(1000):
            k = i + 1
            t = 4 * ((1000-k)/1000)
            denominator = 0
            for d in range(4):
                denominator = denominator + np.exp(Q_a[d]/t)
            for a in range(4):
                action_s[a] = (np.exp(Q_a[a]/t)) / denominator
            ## Select action.
            # Random chance.
            chance = uniform(0,1)
            begin = 0
            r = 0
            for j in range(4):
                end = begin + action_s[j]
                if ((chance >= begin) and (chance <= end)):
                    mean = mean_arr[j]
                    standard_deviation = standard_deviation_arr[j]
                    reward = np.random.normal(mean, standard_deviation, 1)
                    r = reward[0]
                    # Update
                    chosen_action[j] = chosen_action[j] + 1
                    action_reward[j] = action_reward[j] + reward[0]
                    Q_a[j] = action_reward[j] / chosen_action[j]
                begin = end
            # For average reward measurement.
            cum_result = r + cum_result
            target[i] = target[i] + cum_result
            # For actual Qa estimate over time
            arm1[i] = arm1[i] + Q_a[0]
            arm2[i] = arm2[i] + Q_a[1]
            arm3[i] = arm3[i] + Q_a[2]
            arm4[i] = arm4[i] + Q_a[3]
        # Update action selected number
        for s in range(4):
            histogram[s] = histogram[s] + chosen_action[s]

def transfer_arr(curr, new):
    for i in range(1000):
        new.append(curr[i] / 30)

random_n_bandit()
transfer_arr(result_random, result_r)
greedy_n_bandit(0, greedy, arm1_greedy, arm2_greedy, arm3_greedy, arm4_greedy, greedy_h)
transfer_arr(greedy, greedy_r)
greedy_n_bandit(0.1, greedy_01, arm1_greedy_01, arm2_greedy_01, arm3_greedy_01, arm4_greedy_01, greedy_01_h)
transfer_arr(greedy_01, greedy_01_r)
greedy_n_bandit(0.2, greedy_02, arm1_greedy_02, arm2_greedy_02, arm3_greedy_02, arm4_greedy_02, greedy_02_h)
transfer_arr(greedy_02, greedy_02_r)
soft_max(1, soft_1, arm1_softmax_1, arm2_softmax_1, arm3_softmax_1, arm4_softmax_1, softmax_1_h)
transfer_arr(soft_1, soft_1_r)
soft_max(0.1, soft_01, arm1_softmax_01, arm2_softmax_01, arm3_softmax_01, arm4_softmax_01, softmax_01_h)
transfer_arr(soft_01, soft_01_r)
time_greedy(time_greed, arm1_timegreed, arm2_timegreed, arm3_timegreed, arm4_timegreed, timegreed_h )
transfer_arr(time_greed,time_greed_r)
soft_max_time(soft_maxtime, arm1_soft_maxtime, arm2_soft_maxtime, arm3_soft_maxtime, arm4_soft_maxtime, softmax_time_h)
transfer_arr(soft_maxtime, soft_maxtime_r)

## Second arm thing.
transfer_arr(arm1_random, arm1_random_r)
transfer_arr(arm2_random, arm2_random_r)
transfer_arr(arm3_random, arm3_random_r)
transfer_arr(arm4_random, arm4_random_r)
transfer_arr(arm1_greedy, arm1_greedy_r)
transfer_arr(arm2_greedy, arm2_greedy_r)
transfer_arr(arm3_greedy, arm3_greedy_r)
transfer_arr(arm4_greedy, arm4_greedy_r)
transfer_arr(arm1_greedy_01, arm1_greedy_01_r)
transfer_arr(arm2_greedy_01, arm2_greedy_01_r)
transfer_arr(arm3_greedy_01, arm3_greedy_01_r)
transfer_arr(arm4_greedy_01, arm4_greedy_01_r)
transfer_arr(arm1_greedy_02, arm1_greedy_02_r)
transfer_arr(arm2_greedy_02, arm2_greedy_02_r)
transfer_arr(arm3_greedy_02, arm3_greedy_02_r)
transfer_arr(arm4_greedy_02, arm4_greedy_02_r)
transfer_arr(arm1_softmax_1, arm1_softmax_1_r)
transfer_arr(arm2_softmax_1, arm2_softmax_1_r)
transfer_arr(arm3_softmax_1, arm3_softmax_1_r)
transfer_arr(arm4_softmax_1, arm4_softmax_1_r)
transfer_arr(arm1_softmax_01, arm1_softmax_01_r)
transfer_arr(arm2_softmax_01, arm2_softmax_01_r)
transfer_arr(arm3_softmax_01, arm3_softmax_01_r)
transfer_arr(arm4_softmax_01, arm4_softmax_01_r)
transfer_arr(arm1_timegreed, arm1_timegreed_r)
transfer_arr(arm2_timegreed, arm2_timegreed_r)
transfer_arr(arm3_timegreed, arm3_timegreed_r)
transfer_arr(arm4_timegreed, arm4_timegreed_r)
transfer_arr(arm1_soft_maxtime, arm1_soft_maxtime_r)
transfer_arr(arm2_soft_maxtime, arm2_soft_maxtime_r)
transfer_arr(arm3_soft_maxtime, arm3_soft_maxtime_r)
transfer_arr(arm4_soft_maxtime, arm4_soft_maxtime_r)

# Plotting 1.1 - Graph 1.
plt.plot(result_r, label="Random")
plt.plot(greedy_r, label="Greedy with epsilon = 0")
plt.plot(greedy_01_r, label="Greedy with epsilon = 0.1")
plt.plot(greedy_02_r, label="Greedy with epsilon = 0.2")
plt.plot(soft_1_r, label="Softmax with t = 1")
plt.plot(soft_01_r, label="Softmax with t = 0.1")
plt.plot(time_greed_r, label="Time greed with epsilon = 1/sqrt(t)")
plt.plot(soft_maxtime_r, label="Softmax with time varying")
plt.xlabel("Time steps")
plt.ylabel("Average reward over time")
plt.title("Cumulative average reward over time for each algorithm")
plt.legend()
plt.show()


# Plotting 1.1 - 2nd thing - one plot per arm.
mean_arm1 = [mean_arr[0]] * 1000
mean_arm2 = [mean_arr[1]] * 1000
mean_arm3 = [mean_arr[2]] * 1000
mean_arm4 = [mean_arr[3]] * 1000


# Arm 1
plt.plot(arm1_random_r, label="Random")
plt.plot(arm1_greedy_r, label="Greedy with epsilon = 0")
plt.plot(arm1_greedy_01_r, label="Greedy with epsilon = 0.1")
plt.plot(arm1_greedy_02_r, label="Greedy with epsilon = 0.2")
plt.plot(arm1_softmax_1_r, label="Softmax with t = 1")
plt.plot(arm1_softmax_01_r, label="Softmax with t = 0.1")
plt.plot(arm1_timegreed_r, label="Time greed with epsilon = 1/sqrt(t)")
plt.plot(arm1_soft_maxtime_r, label="Softmax with time varying")
plt.plot(mean_arm1, label="mean Qa")
plt.xlabel("Time steps")
plt.ylabel("Actual Qa estimate over time")
plt.title("Actual Qa estimate and mean Qa: arm 1")
plt.legend()
plt.show()

# Arm 2
plt.plot(arm2_random_r, label="Random")
plt.plot(arm2_greedy_r, label="Greedy with epsilon = 0")
plt.plot(arm2_greedy_01_r, label="Greedy with epsilon = 0.1")
plt.plot(arm2_greedy_02_r, label="Greedy with epsilon = 0.2")
plt.plot(arm2_softmax_1_r, label="Softmax with t = 1")
plt.plot(arm2_softmax_01_r, label="Softmax with t = 0.1")
plt.plot(arm2_timegreed_r, label="Time greed with epsilon = 1/sqrt(t)")
plt.plot(arm2_soft_maxtime_r, label="Softmax with time varying")
plt.plot(mean_arm2, label="mean Qa")
plt.xlabel("Time steps")
plt.ylabel("Actual Qa estimate over time")
plt.title("Actual Qa estimate and mean Qa: arm 2")
plt.legend()
plt.show()

# Arm 3
plt.plot(arm3_random_r, label="Random")
plt.plot(arm3_greedy_r, label="Greedy with epsilon = 0")
plt.plot(arm3_greedy_01_r, label="Greedy with epsilon = 0.1")
plt.plot(arm3_greedy_02_r, label="Greedy with epsilon = 0.2")
plt.plot(arm3_softmax_1_r, label="Softmax with t = 1")
plt.plot(arm3_softmax_01_r, label="Softmax with t = 0.1")
plt.plot(arm3_timegreed_r, label="Time greed with epsilon = 1/sqrt(t)")
plt.plot(arm3_soft_maxtime_r, label="Softmax with time varying")
plt.plot(mean_arm3, label="mean Qa")
plt.xlabel("Time steps")
plt.ylabel("Actual Qa estimate over time")
plt.title("Actual Qa estimate and mean Qa: arm 3")
plt.legend()
plt.show()

# Arm 4
plt.plot(arm4_random_r, label="Random")
plt.plot(arm4_greedy_r, label="Greedy with epsilon = 0")
plt.plot(arm4_greedy_01_r, label="Greedy with epsilon = 0.1")
plt.plot(arm4_greedy_02_r, label="Greedy with epsilon = 0.2")
plt.plot(arm4_softmax_1_r, label="Softmax with t = 1")
plt.plot(arm4_softmax_01_r, label="Softmax with t = 0.1")
plt.plot(arm4_timegreed_r, label="Time greed with epsilon = 1/sqrt(t)")
plt.plot(arm4_soft_maxtime_r, label="Softmax with time varying")
plt.plot(mean_arm4, label="mean Qa")
plt.xlabel("Time steps")
plt.ylabel("Actual Qa estimate over time")
plt.title("Actual Qa estimate and mean Qa: arm 4")
plt.legend()
plt.show()


# Histogram per action selection
plt.bar((1,2,3,4), random_h)
plt.ylabel("Times selected")
plt.xlabel("Arm number")
plt.title("Action selection for random")
plt.show()

plt.bar((1,2,3,4), greedy_h)
plt.ylabel("Times selected")
plt.xlabel("Arm number")
plt.title("Action selection for greedy epsilon = 0")
plt.show()

plt.bar((1,2,3,4), greedy_01_h)
plt.ylabel("Times selected")
plt.xlabel("Arm number")
plt.title("Action selection for greedy epsilon = 0.1")
plt.show()

plt.bar((1,2,3,4), greedy_02_h)
plt.ylabel("Times selected")
plt.xlabel("Arm number")
plt.title("Action selection for greedy epsilon = 0.2")
plt.show()

plt.bar((1,2,3,4), softmax_1_h)
plt.ylabel("Times selected")
plt.xlabel("Arm number")
plt.title("Action selection for softmax t = 1")
plt.show()

plt.bar((1,2,3,4), softmax_01_h)
plt.ylabel("Times selected")
plt.xlabel("Arm number")
plt.title("Action selection for softmax t = 0.1")
plt.show()

plt.bar((1,2,3,4), timegreed_h)
plt.ylabel("Times selected")
plt.xlabel("Arm number")
plt.title("Action selection  for time greed epsilon = 1/sqrt(t)")
plt.show()

plt.bar((1,2,3,4), softmax_time_h)
plt.ylabel("Times selected")
plt.xlabel("Arm number")
plt.title("Action selection for softmax with time varying")
plt.show()