\contentsline {section}{\numberline {1}Introduction}{2}{section.1}% 
\contentsline {section}{\numberline {2}N-Armed Bandit}{2}{section.2}% 
\contentsline {subsection}{\numberline {2.1}Exercise 1.1}{2}{subsection.2.1}% 
\contentsline {subsubsection}{\numberline {2.1.1}Cumulative reward average over time}{2}{subsubsection.2.1.1}% 
\contentsline {subsubsection}{\numberline {2.1.2}Mean value per arm}{3}{subsubsection.2.1.2}% 
\contentsline {subsubsection}{\numberline {2.1.3}Amount of action selections}{6}{subsubsection.2.1.3}% 
\contentsline {subsection}{\numberline {2.2}Exercise 1.2}{9}{subsection.2.2}% 
\contentsline {subsubsection}{\numberline {2.2.1}Cumulative reward average over time}{10}{subsubsection.2.2.1}% 
\contentsline {subsubsection}{\numberline {2.2.2}Mean value per arm}{10}{subsubsection.2.2.2}% 
\contentsline {subsubsection}{\numberline {2.2.3}Amount of action selections}{12}{subsubsection.2.2.3}% 
\contentsline {subsection}{\numberline {2.3}Exercise 1.3}{15}{subsection.2.3}% 
\contentsline {subsubsection}{\numberline {2.3.1}Cumulative reward average over time}{16}{subsubsection.2.3.1}% 
\contentsline {subsubsection}{\numberline {2.3.2}Mean value per arm}{17}{subsubsection.2.3.2}% 
\contentsline {subsubsection}{\numberline {2.3.3}Amount of action selections}{19}{subsubsection.2.3.3}% 
\contentsline {section}{\numberline {3}Stochastic Reward Game}{22}{section.3}% 
\contentsline {subsection}{\numberline {3.1}Case A}{23}{subsection.3.1}% 
\contentsline {subsubsection}{\numberline {3.1.1}Average reward per episode}{24}{subsubsection.3.1.1}% 
\contentsline {subsubsection}{\numberline {3.1.2}Emperical joint action distribution: Boltzmann}{24}{subsubsection.3.1.2}% 
\contentsline {subsubsection}{\numberline {3.1.3}Emperical joint action distribution: Optimistic Boltzmann}{25}{subsubsection.3.1.3}% 
\contentsline {subsection}{\numberline {3.2}Case B}{25}{subsection.3.2}% 
\contentsline {subsubsection}{\numberline {3.2.1}Average reward per episode}{26}{subsubsection.3.2.1}% 
\contentsline {subsubsection}{\numberline {3.2.2}Emperical joint action distribution: Boltzmann}{26}{subsubsection.3.2.2}% 
\contentsline {subsubsection}{\numberline {3.2.3}Emperical joint action distribution: Optimistic Boltzmann}{27}{subsubsection.3.2.3}% 
\contentsline {subsection}{\numberline {3.3}Case C}{27}{subsection.3.3}% 
\contentsline {subsubsection}{\numberline {3.3.1}Average reward per episode}{28}{subsubsection.3.3.1}% 
\contentsline {subsubsection}{\numberline {3.3.2}Emperical joint action distribution: Boltzmann}{28}{subsubsection.3.3.2}% 
\contentsline {subsubsection}{\numberline {3.3.3}Emperical joint action distribution: Optimistic Boltzmann}{29}{subsubsection.3.3.3}% 
\contentsline {subsection}{\numberline {3.4}Discussion}{29}{subsection.3.4}% 
\contentsline {section}{References}{29}{section*.2}% 
