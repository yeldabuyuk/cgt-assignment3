## Assignment 3 Part 2 Computational Game Theory
## By Yelda Büyük, 0527366, Vrije Universiteit Brussel, 2020-2021

import numpy as np
from random import choice, uniform
import matplotlib.pyplot as plt

mean_dic = {"a1b1c1": 11, "a1b1c2": -30, "a1b1c3": 0, "a1b2c1": -30, "a1b2c2": 7, "a1b2c3": 6, "a1b3c1": 0, "a1b3c2": 0, "a1b3c3": 5, "a2b1c1": 11, "a2b1c2": -3, "a2b1c3": 0, "a2b2c1": -3, "a2b2c2": 7, "a2b2c3": 6, "a2b3c1": 0, "a2b3c2": 0, "a2b3c3": 5, "a3b1c1": 14, "a3b1c2": -50, "a3b1c3": 0, "a3b2c1": -50, "a3b2c2": 4, "a3b2c3": 3, "a3b3c1": 0, "a3b3c2": 0, "a3b3c3": 3}
std_dic = {"a1b1c1": 2, "a1b1c2": 1, "a1b1c3": 1, "a1b2c1": 1, "a1b2c2": 1, "a1b2c3": 1, "a1b3c1": 1, "a1b3c2": 1, "a1b3c3": 1, "a2b1c1": 2, "a2b1c2":1, "a2b1c3": 1, "a2b2c1": 1, "a2b2c2": 1, "a2b2c3": 1, "a2b3c1": 1, "a2b3c2": 1, "a2b3c3":1, "a3b1c1":2 , "a3b1c2":1, "a3b1c3": 1, "a3b2c1": 1, "a3b2c2": 3, "a3b2c3": 1, "a3b3c1": 1, "a3b3c2": 1, "a3b3c3": 1} # 1 = std, 2 = std0, 3= std1


# Per agent
# Calculate weighted Q values (EV)
# Execute action selection strategy (Boltzmann/Advanced) on these weighted Q values.

def game(sigma, sigma1, sigma2, mean_dic, std_dic, isOptimistic):
    average_reward = []
    reward_arr = np.zeros(5000)  
    empirical_data = {"a1": 0, "a2": 0, "a3": 0, "b1": 0, "b2": 0, "b3": 0, "c1": 0, "c2":0, "c3": 0}
    for t in range(30): # 30 trials
        selected_times_dic = {"a1": 1, "a2": 1, "a3": 1, "b1": 1, "b2": 1, "b3": 1, "c1": 1, "c2": 1, "c3": 1}
        total_times_dic = {"a": 3, "b": 3, "c": 3}
        #Per iteration  
        for i in range(5000): # 5000 iterations
            Q_dic = initialize_Q_values(sigma, sigma1, sigma2, mean_dic, std_dic) # Initialize Q values.
            prob_dic = update_prob(selected_times_dic, total_times_dic) # Probability dictionary.    
            reward_key = ""
            action_values = []
            for a in range(3): # Per agent.
                control = a + 1
                weighted_values_agent = calculate_EV(Q_dic, prob_dic, control)
                selection_action_results = softmax_action_selection(weighted_values_agent, 1, control, isOptimistic) # Tau, control
                selected_action_of_agent = selection_action_results[0]
                action_v = selection_action_results[1]
                action_values.append(action_v)
                reward_key = reward_key + selected_action_of_agent
                # Update probabilities.
                selected_times_dic[selected_action_of_agent] = selected_times_dic[selected_action_of_agent] + 1
                total_times_dic[selected_action_of_agent[0]] = total_times_dic[selected_action_of_agent[0]] + 1
            reward = Q_dic[reward_key]
            # Update reward dictionary.
            reward_arr[i] = reward_arr[i] + reward
            #Emperical data for last 10%
            if i >= 4500: 
                a_values = action_values[0]
                b_values = action_values[1]
                c_values = action_values[2]
                for el in range(len(a_values)):
                    key = str(el + 1)
                    empirical_data["a"+key] = empirical_data["a"+key] + a_values[el]
                    empirical_data["b"+key] = empirical_data["b"+key] + b_values[el]
                    empirical_data["c"+key] = empirical_data["c"+key] + c_values[el]
    agent1_dic = calculate_empirical_join(empirical_data, 1)
    agent2_dic = calculate_empirical_join(empirical_data, 2)
    agent3_dic = calculate_empirical_join(empirical_data, 3)
    # Transform np.zeros into regular array with the averages.
    for el in range(len(reward_arr)):
        average_reward.append(reward_arr[el] / 30)
    return [average_reward, agent1_dic, agent2_dic, agent3_dic]

# Procedure to initialize Q values of the given data in the assignment.
def initialize_Q_values(sigma, sigma1, sigma2, mean_dic, std_dic):
    # Initial Q_dic
    Q_dic = {}
    # Standard deviations keys are = 1 (sigma), 2 (sigma1), 3 (sigma2)
    for key in mean_dic:
        mean = mean_dic[key]
        std_code = std_dic[key]
        if std_code == 1:
            Q_value = np.random.normal(mean, sigma, 1)
        elif std_code == 2:
            Q_value = np.random.normal(mean, sigma1, 1)
        else:
            Q_value = np.random.normal(mean, sigma2, 1)
        # Put Q_values in the dictionary.
        Q_dic.update({key: Q_value[0]}) # Add every key. These are unique keys so we do not have to check.
    return Q_dic

# Given the selected times an action has been selected and the total amount of times the 3 actions per agents have been selected, calculate and generate a probability dictionary.
def update_prob(selected_times_dic, total_times_dic):
    prob_dic = {}
    for key in selected_times_dic:
        prob = selected_times_dic[key] / total_times_dic[key[0]]
        prob_dic.update({key: prob})
    return prob_dic

# Calculate the EV given the Q_dic, prob_dic and a control.
def calculate_EV(Q_dic, prob_dic, control):
    EV_dic = {}
    for a in range(3): # A
        EV_value = 0 # Reset
        for b in range(3):
            for c in range(3):
                if control == 1: # A
                    key_a = "a" + str(a + 1)
                    key_b = "b" + str(b + 1)
                    key_c = "c" + str(c + 1)
                    EV_key = key_a
                elif control == 2: # B
                    key_a = "a" + str(b + 1)
                    key_b = "b" + str(a + 1)
                    key_c = "c" + str(c + 1)
                    EV_key = key_b
                else: # C
                    key_a = "a" + str(b + 1)
                    key_b = "b" + str(c + 1)
                    key_c = "c" + str(a + 1)
                    EV_key = key_c
                key = key_a + key_b + key_c
                # Calculate EV
                EV_value = EV_value + (Q_dic[key]*prob_dic[key_b]*prob_dic[key_c])
        # Update EV dic
        EV_dic.update({EV_key: EV_value})
    return EV_dic

# Procedure to execute the action selection strategy. 
# Q_a are the Q values, t is equal to the Tau parameter, control states which agent is playing (1 = agent A, 2 = agent B, 3 = agent C), isOptimized decides wether we use the 
# normal Boltzmann action selection or the optimistic Boltzmann
def softmax_action_selection(Q_a, t, control, isOptimistic):
    denominator = 0
    action_arr = [0,0,0]
    for i in range(len(Q_a)):
        if control == 1:
            key = "a" + str(i + 1)
        elif control == 2:
            key = "b" + str(i + 1)
        else:
            key = "c" + str(i + 1)
        denominator = denominator + np.exp(Q_a[key]/t)
    for i in range(len(Q_a)):
        if control == 1:
            key = "a" + str(i + 1)
        elif control == 2:
            key = "b" + str(i + 1)
        else:
            key = "c" + str(i + 1)
        value = (np.exp(Q_a[key]/t)) / denominator
        action_arr[i] = value
    selected_action = get_action_selected(action_arr, Q_a, control, isOptimistic)
    return selected_action 

# Returns the action that is selected by the agent.
def get_action_selected(action_arr, Q_a, control, isOptimistic):
    action = 0
    if isOptimistic:
        max_action = max(action_arr)
        action = action_arr.index(max_action)
    else:    
        chance = uniform(0,1) # Chance
        begin = 0
        for j in range(len(Q_a)):
            end = begin + action_arr[j]
            if ((chance >= begin) and (chance <= end)):
                action = j
                begin = end
    # Update selected action to a proper format.
    selected_action = ""
    if control == 1:
        selected_action = "a" + str(action + 1)
    elif control == 2:
        selected_action = "b" + str(action + 1)
    else:
        selected_action = "c" + str(action + 1)
    return selected_action, action_arr

def calculate_empirical_join(empirical_data, control):
    tot = 0
    result_dic = {}
    for b in range(3):
        for c in range(3):
            a_key = "a" + str(control)
            b_key = "b" + str(b+1)
            c_key = "c" + str(c+1)
            tot = tot + empirical_data[a_key] + empirical_data[b_key] + empirical_data[c_key]
    for b in range(3):
        for c in range(3):
            a_key = "a" + str(control)
            b_key = "b" + str(b+1)
            c_key = "c" + str(c+1)
            dic_key = a_key + b_key + c_key
            sub = empirical_data[a_key] + empirical_data[b_key] + empirical_data[c_key]
            sub_tot = sub/tot
            if dic_key not in result_dic:
                result_dic.update({dic_key: sub_tot})
    return result_dic
                

# Start a game.
# isOptimistic == True: uses the optimistic Boltzmann action selection strategy.
# isOptimistic == False: uses the regular Boltzmann action selection strategy.
results_game_a = game(2, 2, 2, mean_dic, std_dic, False)
results_game_b = game(0.5, 4, 0.5, mean_dic, std_dic, False )
results_game_c = game(0.5, 0.5, 4, mean_dic, std_dic, False)


average_reward_a = results_game_a[0]
average_reward_b = results_game_b[0]
average_reward_c = results_game_c[0]

agent1_empirical_a = results_game_a[1] # dictionary
agent2_empirical_a = results_game_a[2]
agent3_empirical_a = results_game_a[3]

agent1_empirical_b = results_game_b[1]
agent2_empirical_b = results_game_b[2]
agent3_empirical_b = results_game_b[3]

agent1_empirical_c = results_game_c[1]
agent2_empirical_c = results_game_c[2]
agent3_empirical_c = results_game_c[3]


optimistic_results_game_a = game(2,2,2, mean_dic, std_dic, True)
optimistic_results_game_b = game(0.5, 4, 0.5, mean_dic, std_dic, True)
optimistic_results_game_c = game(0.5, 0.5, 4, mean_dic, std_dic, True)

optimistic_average_reward_a = optimistic_results_game_a[0]
optimistic_average_reward_b = optimistic_results_game_b[0]
optimistic_average_reward_c = optimistic_results_game_c[0]

agent1_optimistic_a = optimistic_results_game_a[1] #dictionary
agent2_optimistic_a = optimistic_results_game_a[2]
agent3_optimistic_a = optimistic_results_game_a[3]

agent1_optimistic_b = optimistic_results_game_b[1]
agent2_optimistic_b = optimistic_results_game_b[2]
agent3_optimistic_b = optimistic_results_game_b[3]

agent1_optimistic_c = optimistic_results_game_c[1]
agent2_optimistic_c = optimistic_results_game_c[2]
agent3_optimistic_c = optimistic_results_game_c[3]

# Plotting  average rewards.
# case A.
plt.plot(average_reward_a, label="Boltzmann")
plt.plot(optimistic_average_reward_a, label="Optimistic Boltzmann")
plt.xlabel("Episode number")
plt.ylabel("Average reward")
plt.title("Average collected reward per episode")
plt.legend()
plt.show()

# case B.
plt.plot(average_reward_b, label="Boltzmann")
plt.plot(optimistic_average_reward_b, label="Optimistic Boltzmann")
plt.xlabel("Episode number")
plt.ylabel("Average reward")
plt.title("Average collected reward per episode")
plt.legend()
plt.show()

# case C.
plt.plot(average_reward_c, label="Boltzmann")
plt.plot(optimistic_average_reward_c, label="Optimistic Boltzmann")
plt.xlabel("Episode number")
plt.ylabel("Average reward")
plt.title("Average collected reward per episode")
plt.legend()
plt.show()

# Made empirical joint action distribution table manually.
# case A.
print("Case A")
print("Boltzman")
print(agent1_empirical_a)
print(agent2_empirical_a)
print(agent3_empirical_a)

print("Optimistic Boltzmann")
print(agent1_optimistic_a)
print(agent2_optimistic_a)
print(agent2_optimistic_a)

print("Case B")
print("Boltzmann")
print(agent1_empirical_b)
print(agent2_empirical_b)
print(agent3_empirical_b)

print("Optimistic Boltzmann")
print(agent1_optimistic_b)
print(agent2_optimistic_b)
print(agent3_optimistic_b)

print("Case C")
print("Boltzmann")
print(agent1_empirical_c)
print(agent2_empirical_c)
print(agent3_empirical_c)

print("Optimistic Boltzmann")
print(agent1_optimistic_c)
print(agent2_optimistic_c)
print(agent3_optimistic_c)